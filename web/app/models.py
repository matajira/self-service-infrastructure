from app import db
from datetime import datetime

class Task(db.Model):
    """ Task represents a task sent by the user """
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(64), index=True )
    username = db.Column(db.String(64), index=True )
    body = db.Column(db.String(300), index=True )
    hash = db.Column(db.String(300), index=True)
    result = db.relationship('Result', backref='task', lazy='dynamic')

    def __repr__(self):
        return '<Task {}>'.format(self.username)   

class Result(db.Model):
    """ Result represents the result of a task """
    id = db.Column(db.Integer, primary_key=True)
    server  = db.Column(db.String(140))
    body = db.Column(db.String(3000), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))

    def __repr__(self):
        return '<Task {0} Server {1}>'.format(self.id, self.server) 

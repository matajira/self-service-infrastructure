from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, SelectMultipleField
from wtforms.widgets import TextArea
from wtforms.validators import DataRequired

class UserCreationForm(FlaskForm):
    """ This class correspondes to all the fields of the User Cretion Form """
    username = StringField('Username', validators=[DataRequired()])
    uid = StringField('UID', validators=[DataRequired()])
    groups = StringField('Groups', validators=[DataRequired()])
    servers = SelectMultipleField(u'Servers', choices=[('server', 'server')])
    public_key = TextAreaField('Public Key', widget=TextArea())
    submit = SubmitField('Create')

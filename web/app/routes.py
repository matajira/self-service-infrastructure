import hashlib
import time
from datetime import datetime
from flask import render_template, request
from app import app
from app.forms import UserCreationForm
from app import db
from app.models import Task, Result
from tasks import app_celery
from tasks import create_user



@app.route('/')
@app.route('/index')
def index():
    """ Renders the index.html template """
    return render_template('index.html', title='Home')


@app.route('/create', methods=['GET', 'POST'])
def user_creation():
    """ Manages the user creation form

    If GET renders CreateUserForm
    If POST processes the CreateUserForm and render after_create_user.html

    Returns:
        Rendered HTML of the CreateUserForm
        Rendered HTML of the After_create_user

    """
    def user_pressed_create_user_button(form):
        """ Return if user pressed the create-user_button (POST request) """
        return form.validate_on_submit()

    form = create_user_creation_form_and_append_active_server_choices()
    if user_pressed_create_user_button(form):
        return process_the_user_creation_request_and_render_html(request)
    return render_template('create_user.html', title='Create User',
                           form=form, num_server_rows=len(form.servers.choices))


def create_user_creation_form_and_append_active_server_choices():
    """ Create UserCreationForm and appends the active server choices

    Returns:
        UserCreationForm wiht the active server choices appended
    """
    form = UserCreationForm()
    form.servers.choices = get_connected_server_value_and_labels()
    return form


def get_connected_server_value_and_labels():
    """ Gets the current connected servers with labels to Celery/RabbitMQ

    Returns:
        List of tuples of servers (and labels) connected to Celery/RabbitMQ
    """
    def get_list_of_connected_users_at_server_from_celery():
        """ Gets list of connected user@server from celery """
        return app_celery.control.inspect().active().keys()

    def create_list_of_server_values_and_labels(list_of_user_at_server):
        """ Create list of servers with the corresponding labels"""
        list_of_server_values_and_labels = [(get_server_name(i), get_server_name(i)) for i in list_of_user_at_server]
        list_of_server_values_and_labels.sort()
        return list_of_server_values_and_labels

    def get_server_name(user_at_server):
        """ Gets the server from user@server text """
        server_name_index = 1
        return user_at_server.split("@")[server_name_index]

    list_of_connected_users_at_servers = get_list_of_connected_users_at_server_from_celery()
    list_of_connected_servers_with_labels = create_list_of_server_values_and_labels(list_of_connected_users_at_servers)
    return list_of_connected_servers_with_labels


def process_the_user_creation_request_and_render_html(user_creation_request):
    """ Processes the user creation request

    Gets the fields provided by the user and then sends the task to the servers through Celery.
    Ends by rendering the after_create_user.html

    Returns:
        Rendered HTML of after_create_user.html
    """

    def process_the_user_creation_request(user_creation_request):
        """ Gets the values provided by the user and sends the instruction to create the user """
        def create_organized_request_with_values_provided_by_the_user(user_creation_request):
            user_creation_type_of_request = "Create User"
            username = user_creation_request.form.get("username")
            uid = user_creation_request.form.get("uid")
            groups = user_creation_request.form.get("groups")
            servers = sorted(user_creation_request.form.getlist("servers"))
            public_key = user_creation_request.form.get("public_key")
            organized_request = dict(type=user_creation_type_of_request, username=username, uid=uid,
                                                 groups=groups, servers=servers, public_key=public_key, body=None, hash=None)
            return organized_request

        def add_task_body_and_hash(organized_request):
            task_body = "{0} {1} {2} {3} {4} {5} {6}".format("create",
                                                             str(organized_request["username"]),
                                                             str(organized_request["uid"]),
                                                             str(organized_request["groups"]),
                                                             str(organized_request["servers"]),
                                                             str(organized_request["public_key"]),
                                                             str(datetime.now()))
            task_hash = hashlib.sha224(task_body.encode()).hexdigest()
            organized_request["body"] = task_body
            organized_request["hash"] = task_hash
            return organized_request

        def store_task_in_db_and_initialize_results_in_db(organized_request):
            username_of_request_sender = "Admin"
            initial_status_of_result = "Pending"
            task = Task(type=organized_request["type"], username=username_of_request_sender,
                        body=organized_request["body"], hash=organized_request["hash"])
            db.session.add(task)
            for i in organized_request["servers"]:
                result = Result(server=i, body=initial_status_of_result, task=task)
                db.session.add(result)
            db.session.commit()

        def execute_user_creation_task_in_servers_and_wait(organized_request):
            create_user.delay(organized_request["username"], organized_request["uid"],
                              organized_request["groups"], organized_request["public_key"],
                              organized_request["servers"], organized_request["hash"])

        def get_task_results(organized_request):
            time.sleep(2)
            current_task = Task.query.filter_by(hash=organized_request["hash"]).first()
            current_task_results = current_task.result.all()
            return current_task_results

        organized_request = create_organized_request_with_values_provided_by_the_user(user_creation_request)
        organized_request = add_task_body_and_hash(organized_request)
        store_task_in_db_and_initialize_results_in_db(organized_request)
        execute_user_creation_task_in_servers_and_wait(organized_request)
        task_results = get_task_results(organized_request)
        return task_results

    results = process_the_user_creation_request(user_creation_request)
    return render_template('after_create_user.html', title='Create User', task_results=results)

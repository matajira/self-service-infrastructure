import subprocess
from celery import Celery
from kombu import Exchange, Queue
from kombu.common import Broadcast
import shlex


BROKER_URL = 'amqp://camilo:camilo@rabbitmq//'

celery_exhange = Exchange('q1', type='fanout')
b = Broadcast(name='q1')

class CeleryConf:
    """ Celery configuration file """
    CELERY_IMPORTS = ('tasks')
    CELERY_QUEUES = (Broadcast('q1'),)
    CELERY_ROUTES = {
        'tasks.create_user': {'queue': 'q1'}
    }

app_celery = Celery('appcelery', broker=BROKER_URL,)
app_celery.config_from_object(CeleryConf())

@app_celery.task
def create_user(username, uid, group, auth_key, servers, task_hash):
    """ Creates a user in the worker server 

    Creates a user by calling the /usr/sbin/create_user_and_add_authorized_keys.py
    that should be present in the script. Assumes sudo privileges for the celery user.

    Args:
        username: The user's username
	uid: The user's uid
	group: Comma separated string with the groups in which the user belongs
	auth_key: The user's public key
        servers: List of servers in which the user should be created
        task_hash: The identifier (a hash) of the 'parent' task.

    Returns:
	USER_CREATED_MESSAGE constant if the user was created succesfully.
        Exception's message, in case the user was not created.
        'User Not created' if the server was not supposed to created the user.
    """
    import os
    import sys
    from app import db
    from app.models import Task, Result
    
    def this_server_in_list_of_servers_that_should_create_the_user(servers):
        """ Returns if the server is in the list of servers that should create the user """
        return os.environ["HOSTNAME"] in servers

    def execute_user_creation_script(username, uid, group, auth_key):
        """ Execute's the user creation script with sudo privileges """
        create_user_system_command = "sudo /usr/sbin/create_user_and_add_authorized_keys.py --username {0} --group {1} --uid {2} --key \"{3}\"".format(shlex.quote(username), shlex.quote(group), shlex.quote(uid), shlex.quote(auth_key))
        user_creation_result = subprocess.run(create_user_system_command, shell=True, check=True, stdout=subprocess.PIPE).stdout
        return user_creation_result

    def persist_in_db_task_results(user_creation_result, task_hash):
        """ Persists the user creation result in the table 'result' where the servername and task_hash match """
        current_task = Task.query.filter_by(hash=task_hash).first()
        current_task_results = current_task.result.filter_by(server=os.environ["HOSTNAME"]).first()
        current_task_results.body = user_creation_result
        db.session.commit()

    if this_server_in_list_of_servers_that_should_create_the_user(servers):
        user_creation_result = execute_user_creation_script(username, uid, group, auth_key)
        persist_in_db_task_results(user_creation_result, task_hash)
        return user_creation_result 
    return "User NOT created"
